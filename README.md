# Ejecutar

mvn spring-boot:run

# compilar

mvn compile jib:dockerBuild

# ejecutar local

docker run -p3301:3301 springbootdemo
