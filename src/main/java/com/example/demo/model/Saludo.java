package com.example.demo.model;

import io.micrometer.core.lang.NonNull;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Saludo {
    @NonNull
    private long id;
    
    @NonNull
    private String content;
}
