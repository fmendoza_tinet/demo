package com.example.demo.controller;

import java.util.concurrent.atomic.AtomicLong;

import com.example.demo.model.Saludo;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SaludoController {

    private static final Logger LOGGER = LogManager.getLogger(SaludoController.class);
    private static final String TEMPLATE = "Bienvenido, %s! chan";
    private final AtomicLong counter = new AtomicLong();

    @GetMapping("/saludar")
    public Saludo saludar(@RequestParam(value = "nombre", defaultValue = "Mundo") String nombre) {
        LOGGER.debug("Parametro recibido: {}", nombre);
        return new Saludo(counter.incrementAndGet(), String.format(TEMPLATE, nombre));
    }
}
