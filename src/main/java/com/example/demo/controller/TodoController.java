package com.example.demo.controller;

import java.util.Arrays;

import com.example.demo.model.Todo;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("api")
public class TodoController {

    private static final Logger LOGGER = LogManager.getLogger(TodoController.class);

    Todo[] todoList = {new Todo("Todo 1"), new Todo("Todo 2")};

    @GetMapping("/todos")
    public Todo[] getTodos() {
        LOGGER.info("get Todos");
        return todoList;
    }

    @PostMapping("/addTodo")
    public Todo[] addTodo() {
        LOGGER.info("add Todo");

        Todo[] newTodoList = Arrays.copyOf(todoList, todoList.length + 1);
        newTodoList[todoList.length] = new Todo("Todo 3");

        return newTodoList;
    }
}
